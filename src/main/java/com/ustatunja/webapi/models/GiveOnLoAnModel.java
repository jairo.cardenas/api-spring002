package com.ustatunja.webapi.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "give_on_loan")
public class GiveOnLoAnModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_give_on_loan")
    private Integer id_give_on_loan;

    @ManyToOne(optional = false, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_book",  referencedColumnName = "id_book", nullable = false)
    private BookModel  id_book;

    @ManyToOne(optional = false, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_client", referencedColumnName = "id_student", nullable = false)
    private StudentModel id_client;

    @Basic(optional = false)
    @NotNull
    @Column(name = "date_give_on_loan", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date_give_on_loan;

    public GiveOnLoAnModel() {
    }

    public GiveOnLoAnModel(Integer id_give_on_loan, BookModel id_book, StudentModel id_client, @NotNull Date date_give_on_loan) {
        this.id_give_on_loan = id_give_on_loan;
        this.id_book = id_book;
        this.id_client = id_client;
        this.date_give_on_loan = date_give_on_loan;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId_give_on_loan() {
        return id_give_on_loan;
    }

    public void setId_give_on_loan(Integer id_give_on_loan) {
        this.id_give_on_loan = id_give_on_loan;
    }

    public BookModel getId_book() {
        return id_book;
    }

    public void setId_book(BookModel id_book) {
        this.id_book = id_book;
    }

    public StudentModel getId_client() {
        return id_client;
    }

    public void setId_client(StudentModel id_client) {
        this.id_client = id_client;
    }

    public Date getDate_give_on_loan() {
        return date_give_on_loan;
    }

    public void setDate_give_on_loan(Date date_give_on_loan) {
        this.date_give_on_loan = date_give_on_loan;
    }

    @Override
    public String toString() {
        return "GiveOnLoAnModel{" +
                "id_give_on_loan=" + id_give_on_loan +
                ", id_book=" + id_book +
                ", id_client=" + id_client +
                ", date_give_on_loan=" + date_give_on_loan +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiveOnLoAnModel that = (GiveOnLoAnModel) o;
        return Objects.equals(getId_give_on_loan(), that.getId_give_on_loan()) && Objects.equals(getId_book(), that.getId_book()) && Objects.equals(getId_client(), that.getId_client()) && Objects.equals(getDate_give_on_loan(), that.getDate_give_on_loan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_give_on_loan(), getId_book(), getId_client(), getDate_give_on_loan());
    }
}
