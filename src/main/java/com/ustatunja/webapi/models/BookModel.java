package com.ustatunja.webapi.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "book")
public class BookModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private Integer id_book;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title", nullable = false)
    private String title;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "idiom", nullable = false)
    private String idiom;

    @Basic(optional = false)
    @NotNull
    @Column(name = "price", nullable = false, precision = 4, scale = 3)
    private Float price;

    @Basic(optional = false)
    @NotNull
    @Min(1)
    @Max(3)
    @Column(name = "stock", nullable = false)
    private Short stock;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "id_give_on_loan")
    private Collection<GiveOnLoAnModel> giveOnLoAnModelColection;

    public BookModel() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId_book() {
        return id_book;
    }

    public void setId_book(Integer id_book) {
        this.id_book = id_book;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdiom() {
        return idiom;
    }

    public void setIdiom(String idiom) {
        this.idiom = idiom;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Short getStock() {
        return stock;
    }

    public void setStock(Short stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "id_book=" + id_book +
                ", title='" + title + '\'' +
                ", idiom='" + idiom + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookModel bookModel = (BookModel) o;
        return Objects.equals(getId_book(), bookModel.getId_book()) && Objects.equals(getTitle(), bookModel.getTitle()) && Objects.equals(getIdiom(), bookModel.getIdiom()) && Objects.equals(getPrice(), bookModel.getPrice()) && Objects.equals(getStock(), bookModel.getStock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_book(), getTitle(), getIdiom(), getPrice(), getStock());
    }
}
