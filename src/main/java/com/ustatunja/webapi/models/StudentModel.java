package com.ustatunja.webapi.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "student")
public class StudentModel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_student")
    private Integer id_student;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name", nullable = false)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Email(message = "Email invalido")
    @Column(name = "email", nullable = false)
    private String email;

    @Basic(optional = false)
    @NotNull
//    @Size(min = 1, max = 50)
    @Column(name = "identification", nullable = false, unique = true, length = 50)
    private String identification;

    @Basic(optional = false)
    @NotNull
    @Column(name = "studentState", nullable = false)
    private Boolean studentState;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "id_give_on_loan")
    private Collection<GiveOnLoAnModel> giveOnLoAnModelColection;


    public StudentModel() {
    }

    public StudentModel(Integer id_student, @NotNull @Size(min = 1, max = 50) String name, @NotNull @Size(min = 1, max = 50) String lastname, @NotNull @Size(min = 1, max = 150) @Email(message = "Email invalido") String email, @NotNull String identification, @NotNull boolean studentState) {
        this.id_student = id_student;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.identification = identification;
        this.studentState = studentState;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId_student() {
        return id_student;
    }

    public void setId_student(Integer id_student) {
        this.id_student = id_student;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public boolean isStudentState() {
        return studentState;
    }

    public void setStudentState(boolean studentState) {
        this.studentState = studentState;
    }

    @Override
    public String toString() {
        return "StudentModel{" +
                "id_student=" + id_student +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", identification='" + identification + '\'' +
                ", studentState=" + studentState +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentModel that = (StudentModel) o;
        return isStudentState() == that.isStudentState() && Objects.equals(getId_student(), that.getId_student()) && Objects.equals(getName(), that.getName()) && Objects.equals(getLastname(), that.getLastname()) && Objects.equals(getEmail(), that.getEmail()) && Objects.equals(getIdentification(), that.getIdentification());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_student(), getName(), getLastname(), getEmail(), getIdentification(), isStudentState());
    }
}
